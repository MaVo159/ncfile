#![feature(bool_to_option)]

mod browser;
mod download;
mod hello;
mod switchboard;
mod upload;

use crate::browser::Browser;
use crate::download::listen_download;
use crate::hello::Hello;
use crate::switchboard::Switchboard;
use crate::upload::listen_upload;
use async_std::io::ErrorKind;
use async_std::net::{TcpListener, TcpStream};
use async_std::path::PathBuf;
pub use async_std::prelude::*;
pub use async_std::sync::Arc;
use async_std::task::spawn_local;
pub use futures::join;
use log::LevelFilter;
use simple_logger::SimpleLogger;
use std::panic::catch_unwind;
use structopt::StructOpt;
pub use termion::color::{Bg, Black, Blue, Cyan, Fg, Green, Red, White, Yellow};
pub use termion::event::Key;
use termion::event::{parse_event, Event};
use termion::{clear, cursor};

type Error = Box<dyn std::error::Error>;
type Result<T> = std::result::Result<T, Error>;

#[derive(StructOpt, Clone, Debug)]
pub struct Opts {
    domain: String,
    password: String,
    dir: PathBuf,
}

#[async_std::main]
async fn main() -> Result<()> {
    SimpleLogger::new().with_level(LevelFilter::Info).init()?;

    let user_listener = TcpListener::bind("0.0.0.0:1234").await?;
    let download_listener = TcpListener::bind("0.0.0.0:1235").await?;
    let upload_listener = TcpListener::bind("0.0.0.0:1236").await?;
    let switchboard = Switchboard::new();

    let opts = Arc::new(Opts::from_args());
    join! {
         listen_user(user_listener, &opts, &switchboard),
         listen_download(download_listener, &opts, &switchboard),
         listen_upload(upload_listener, &opts),
    };
    Ok(())
}

async fn listen_user(listener: TcpListener, opts: &Arc<Opts>, switchboard: &Switchboard) {
    while let Ok((tcp, addr)) = listener.accept().await {
        let state = State::new(opts.clone(), switchboard.clone());
        spawn_local(async move {
            log::info!("new user connection from: {:?}", addr);
            if let Err(err) = handle_user(tcp, state).await {
                log::error!("fatal error: {:?}", err)
            }
        });
    }
}

async fn handle_user(mut tcp: TcpStream, mut state: State) -> Result<()> {
    loop {
        state.view(&mut tcp).await?;
        state = state.update(&mut tcp).await?;
    }
}

pub async fn key(tcp: &mut TcpStream) -> Result<Key> {
    let mut input = [0u8; 8];
    let mut len = 0;
    loop {
        match tcp.read(&mut input[len..len + 1]).await? {
            0 => return Err(std::io::Error::from(ErrorKind::UnexpectedEof).into()),
            n => len += n,
        }
        let event =
            catch_unwind(|| parse_event(input[0], &mut input[1..len].iter().map(|b| Ok(*b))));
        match event {
            Ok(Ok(Event::Key(key))) => return Ok(key),
            Ok(Err(err)) => {
                if len == input.len() {
                    return Err(err.into());
                }
            }
            _ => {}
        }
    }
}

#[derive(Clone, Debug)]
enum State {
    Hello(Hello),
    Browser(Browser),
}

impl State {
    fn new(opts: Arc<Opts>, switchboard: Switchboard) -> Self {
        Self::Hello(Hello {
            switchboard,
            opts,
            password: String::new(),
        })
    }
    async fn update(self, tcp: &mut TcpStream) -> Result<State> {
        match self {
            State::Hello(state) => state.update(tcp).await,
            State::Browser(state) => state.update(tcp).await,
        }
    }
    async fn view(&self, tcp: &mut TcpStream) -> Result<()> {
        write!(
            tcp,
            "{}{}{}{}",
            Bg(Black),
            Fg(White),
            clear::All,
            cursor::Goto(1, 1)
        )
        .await?;
        match self {
            State::Hello(state) => state.view(tcp).await?,
            State::Browser(state) => state.view(tcp).await?,
        }
        writeln!(tcp, "").await?;
        tcp.flush().await?;
        Ok(())
    }
}
