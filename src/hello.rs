use crate::*;

#[derive(Clone, Debug)]
pub struct Hello {
    pub switchboard: Switchboard,
    pub opts: Arc<Opts>,
    pub password: String,
}

impl Hello {
    pub(crate) async fn update(mut self, tcp: &mut TcpStream) -> Result<State> {
        let key = key(tcp).await?;
        match key {
            Key::Char('\r') => {}
            Key::Char('\n') => {
                if self.password == self.opts.password {
                    let opts = self.opts.clone();
                    let switchboard = self.switchboard.clone();
                    return Ok(Browser::new(opts, switchboard).await?);
                }
            }
            Key::Char(c) => self.password.push(c),
            Key::Backspace => self.password.pop().map_or((), |_| ()),
            _ => {}
        }
        Ok(State::Hello(self))
    }
    pub(crate) async fn view(&self, tcp: &mut TcpStream) -> Result<()> {
        writeln!(tcp, "speak {}▪ and <enter>", &self.password).await?;
        writeln!(
            tcp,
            "\n\nUse the following command for an optimal experience:"
        )
        .await?;
        writeln!(
            tcp,
            "{}(trap 'stty sane echo' 0; stty -icanon -echo && nc {} 1234)",
            Fg(Yellow),
            self.opts.domain
        )
        .await?;
        Ok(())
    }
}
