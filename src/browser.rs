use crate::*;
use async_std::channel::Sender;
use async_std::fs::read_dir;
use async_std::path::PathBuf;
use async_std::task::spawn;
use ring::rand::{SecureRandom, SystemRandom};
use std::ascii::escape_default;

#[derive(Clone, Debug)]
pub struct Browser {
    pub opts: Arc<Opts>,
    pub dir: PathBuf,
    pub entries: Vec<Entry>,
    pub selected: Option<isize>,
    pub channel: u128,
    pub switchboard: Switchboard,
    pub sender: Sender<(PathBuf, bool)>,
}

impl Drop for Browser {
    fn drop(&mut self) {
        let switchboard = self.switchboard.clone();
        let channel = self.channel;
        spawn(async move {
            switchboard.remove(channel).await;
        });
    }
}

#[derive(Clone, Debug)]
pub struct Entry {
    name: PathBuf,
    dir: bool,
}

impl Browser {
    pub(crate) async fn new(opts: Arc<Opts>, switchboard: Switchboard) -> Result<State> {
        let dir = PathBuf::new();
        let entries = Vec::new();
        let selected = None;
        let mut channel = [0u8; 16];
        SystemRandom::new().fill(&mut channel).unwrap();
        let channel = u128::from_le_bytes(channel);
        println!("{:x}", channel);
        let sender = switchboard.set(channel).await;
        let mut state = Browser {
            opts,
            dir,
            entries,
            selected,
            channel,
            switchboard,
            sender,
        };

        state.refresh().await?;
        Ok(State::Browser(state))
    }
    pub(crate) async fn update(self, tcp: &mut TcpStream) -> Result<State> {
        let key = key(tcp).await?;
        self.update_key(key).await
    }
    pub(crate) async fn update_key(mut self, key: Key) -> Result<State> {
        let n = self.entries.len() as isize;
        match key {
            Key::Up => *self.selected.get_or_insert(n) -= 1,
            Key::Down => *self.selected.get_or_insert(-1) += 1,
            Key::Backspace => {
                self.dir.pop();
                self.refresh().await?;
            }
            Key::Char('\n') => match self.selected {
                Some(selected) => {
                    if self.entries[selected as usize].dir {
                        self.dir.push(&self.entries[selected as usize].name);
                        self.refresh().await?;
                    }
                }
                None => {}
            },
            Key::Char('d') => match self.selected {
                Some(selected) => {
                    let entry = &self.entries[selected as usize];
                    let path = self.dir.join(&entry.name);
                    self.sender.send((path, entry.dir)).await.unwrap()
                }
                None => {}
            },
            _ => {}
        }
        if self.selected == Some(-1) || self.selected == Some(n) {
            self.selected = None;
        }
        Ok(State::Browser(self))
    }
    pub(crate) async fn view(&self, tcp: &mut TcpStream) -> Result<()> {
        writeln!(
            tcp,
            "{}Current directory: /{}\n",
            Fg(Red),
            self.dir.display()
        )
        .await?;
        for (i, entry) in self.entries.iter().enumerate() {
            match (Some(i as isize) != self.selected).then(|| entry.dir) {
                None => write!(tcp, "{}{}", Bg(Yellow), Fg(Black)).await?,
                Some(true) => write!(tcp, "{}{}", Bg(Black), Fg(Cyan)).await?,
                Some(false) => write!(tcp, "{}{}", Bg(Black), Fg(Green)).await?,
            }
            match entry.dir {
                true => writeln!(tcp, "{}/", entry.name.display()).await?,
                false => writeln!(tcp, "{}", entry.name.display()).await?,
            }
        }
        writeln!(tcp, "{}{}", Bg(Black), Fg(White)).await?;

        self.write_help(tcp).await?;
        Ok(())
    }
    pub(crate) async fn write_help(&self, tcp: &mut TcpStream) -> Result<()> {
        Self::write_key_help(tcp, "↵", "enter dir").await?;
        Self::write_key_help(tcp, "←", "parent dir").await?;
        Self::write_key_help(tcp, "d", "download").await?;
        writeln!(
            tcp,
            "\n\nReceive queued downloads in the working directory with:"
        )
        .await?;
        write!(tcp, "{}while (printf \"", Fg(Yellow)).await?;
        for b in &self.channel.to_le_bytes() {
            match b {
                48..=57 | 64..=90 | 97..=122 => write!(tcp, "{}", escape_default(*b)).await?,
                _ => write!(tcp, "\\x{:x}", *b).await?,
            }
        }
        writeln!(
            tcp,
            "\" | nc {} 1235 | tar -xv); do echo done; done{}",
            self.opts.domain,
            Fg(White)
        )
        .await?;
        writeln!(tcp, "\nUpload files or directories with:").await?;
        write!(
            tcp,
            "{}bash -c 'tar -cv \"${{0}}\" \"${{@}}\" | cat <(printf {}) - | nc {} 1236'{} <somefile>",
            Fg(Yellow),
            self.opts.password,
            self.opts.domain,
            Fg(White)
        )
        .await?;
        Ok(())
    }
    pub(crate) async fn write_key_help(tcp: &mut TcpStream, key: &str, desc: &str) -> Result<()> {
        write!(
            tcp,
            "{}{} {} {}{}{} ",
            Bg(White),
            Fg(Black),
            key,
            Bg(Black),
            Fg(White),
            desc
        )
        .await?;
        Ok(())
    }
    pub(crate) async fn refresh(&mut self) -> Result<()> {
        self.entries.clear();
        self.selected = None;
        let mut entries = read_dir(self.opts.dir.join(&self.dir)).await?;
        while let Some(entry) = entries.next().await {
            let entry = entry?;
            let meta = entry.metadata().await?;
            if meta.is_dir() == meta.is_file() {
                continue;
            }
            let name = PathBuf::from(entry.file_name());
            let dir = meta.is_dir();
            self.entries.push(Entry { name, dir })
        }
        Ok(())
    }
}
