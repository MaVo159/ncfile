use crate::*;
use async_std::prelude::*;
use async_tar::Archive;

pub async fn listen_upload(listener: TcpListener, opts: &Arc<Opts>) {
    while let Ok((mut tcp, addr)) = listener.accept().await {
        let opts = opts.clone();
        spawn_local(async move {
            log::info!("new upload connection from: {:?}", addr);
            if let Err(err) = handle_upload(&mut tcp, opts).await {
                log::error!("fatal error: {:?}", err)
            }
        });
    }
}

async fn handle_upload(tcp: &mut TcpStream, opts: Arc<Opts>) -> Result<()> {
    let mut buffer = [0u8].repeat(opts.password.as_bytes().len());
    tcp.read_exact(buffer.as_mut_slice()).await?;
    if opts.password.as_bytes() == buffer.as_slice() {
        use futures::AsyncReadExt;
        let (r, mut w) = tcp.split();
        Archive::new(r).unpack(&opts.dir).await?;
        writeln!(w, "\nSuccessfully transferred all files!").await?;
    }
    Ok(())
}
