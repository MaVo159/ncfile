use crate::*;
use async_std::channel::Receiver;
use async_tar::Builder;

pub async fn listen_download(listener: TcpListener, opts: &Arc<Opts>, switchboard: &Switchboard) {
    while let Ok((mut tcp, addr)) = listener.accept().await {
        let switchboard = switchboard.clone();
        let opts = opts.clone();
        spawn_local(async move {
            log::info!("new download connection from: {:?}", addr);
            if let Err(err) = handle_download(&mut tcp, opts, switchboard).await {
                log::error!("fatal error: {:?}", err)
            }
        });
    }
}

async fn handle_download(
    tcp: &mut TcpStream,
    opts: Arc<Opts>,
    switchboard: Switchboard,
) -> Result<()> {
    let mut channel = [0u8; 16];
    tcp.read_exact(&mut channel).await?;
    let channel = u128::from_le_bytes(channel);
    println!("{:x}", channel);
    match switchboard.get(channel).await {
        None => log::warn!("user session not found"),
        Some(receiver) => send_tar(tcp, opts, receiver).await?,
    }
    Ok(())
}

async fn send_tar(
    tcp: &mut TcpStream,
    opts: Arc<Opts>,
    receiver: Receiver<(PathBuf, bool)>,
) -> Result<()> {
    let mut tar = Builder::new(tcp);
    let (path, dir) = receiver.recv().await?;
    let local_path = opts.dir.join(&path);
    let path = path.file_name().unwrap();
    match dir {
        false => tar.append_path_with_name(local_path, &path).await?,
        true => tar.append_dir_all(&path, local_path).await?,
    }
    tar.into_inner().await?.flush().await?;
    Ok(())
}
