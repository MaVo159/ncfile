use crate::*;
use async_std::channel::{unbounded, Receiver, Sender};
use async_std::sync::Mutex;
use std::collections::HashMap;

#[derive(Clone, Debug)]
pub struct Switchboard(Arc<Mutex<HashMap<u128, Receiver<(PathBuf, bool)>>>>);

impl Switchboard {
    pub fn new() -> Self {
        Self(Arc::new(Mutex::new(HashMap::new())))
    }
    pub async fn get(&self, channel: u128) -> Option<Receiver<(PathBuf, bool)>> {
        self.0.lock().await.get(&channel).cloned()
    }
    pub async fn set(&self, channel: u128) -> Sender<(PathBuf, bool)> {
        let (sender, receiver) = unbounded();
        self.0.lock().await.insert(channel, receiver);
        sender
    }
    pub async fn remove(&self, channel: u128) {
        self.0.lock().await.remove(&channel);
    }
}
